import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          <code>project-brando</code> coming soon...
        </p>
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Development by:
          <a className="App-link"
             href="http://matthewlamperski.com"
             target="_blank"
             rel="noopener noreferrer">Lamprski</a>
        </p>
        <p>
          Contact: 352.445.0365
        </p>
      </header>
    </div>
  );
}

export default App;
